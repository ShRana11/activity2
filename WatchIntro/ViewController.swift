//
//  ViewController.swift
//  WatchIntro
//
//  Created by Sukhwinder Rana on 2019-06-18.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
      
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if(WCSession.isSupported()){
            print("phone supported")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
             print("phone not supported")
        }
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        
        print(" Phone side message : \(message)")
        
        self.output.text = "\(message["Wmess"]!)"
        
    }

    @IBOutlet weak var label1: UILabel!
    
    
    @IBOutlet weak var output: UILabel!
    
   
    @IBAction func button(_ sender: Any) {
        output.text = "Data has been Sent From Mobile"
        
        if(WCSession.default.isReachable){
            print("watch fopund")
            let m = [
                "Pmess":"Hello Watch Side",
            ]
            var d = WCSession.default.receivedApplicationContext
            print("received:\(d)")
            WCSession.default.sendMessage(m, replyHandler:nil)
            
        }else{
           print("watch not fopund")
        }
    }
    
    
    
    
}

