//
//  InterfaceController.swift
//  WatchIntro WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-06-18.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import WatchKit
import Foundation
import  WatchConnectivity


class InterfaceController: WKInterfaceController, WCSessionDelegate {
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?){
   
}
    
    
    var button:Bool = false
    
    @IBAction func watchButton() {
        print("Watch side button pressed")
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        if (WCSession.default.isReachable) {
            let watchAck = ["Wmess" : "Hello Phone Side"]
            let session = WCSession.default
            session.sendMessage(watchAck, replyHandler: { (replyData) in
                print("Acknowledge From watch is : \(replyData)")
                WKInterfaceDevice.current().play(WKHapticType.notification)
            }, errorHandler: { (error) in
                print("Acknowledge From watch is : \(error)")
            })
        }
            
        else {
            print("phone Side not support")
        }
        
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("got message")
        var data = message["Pmess"]
        print("data is \(data!)")
        watchLabel.setText(data as! String)
//        var ack = [
//            "result":"reached"
//        ]
//        WCSession.default.sendMessage(ack, replyHandler: nil, errorHandler: nil)
    }
  
    @IBOutlet weak var watchLabel: WKInterfaceLabel!
    
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        if (WCSession.isSupported()){
            print("Watch supported")
            let session = WCSession.default
            session.delegate = self
            session.activate()
            
        }
        else{
            
            print("watch not supported")
        }
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    
}
