//
//  Screen2InterfaceController.swift
//  WatchIntro WatchKit Extension
//
//  Created by Sukhwinder Rana on 2019-06-18.
//  Copyright © 2019 Sukhwinder Rana. All rights reserved.
//

import WatchKit
import Foundation


class Screen2InterfaceController: WKInterfaceController {

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        print("i a on screen 2")
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

}
